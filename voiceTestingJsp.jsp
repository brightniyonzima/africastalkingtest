<%-- 
    Document   : voiceTestingJsp
    Created on : 13-Nov-2018, 15:56:08
    Author     : lenovo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>Test Page</title>
        </head>
        <body>
            <h1><h:outputText value="Hello MUST tech section!"/></h1>
            <%
               VoiceTesting voiceinstance = new VoiceTesting();
               voiceinstance.testMethod();
               out.print(voiceinstance.testMethod());
            %>
        </body>
    </html>
</f:view>
