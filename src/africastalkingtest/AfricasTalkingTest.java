package africastalkingtest;

/* Import SDK classes */
import com.africastalking.Callback;
import com.africastalking.SmsService;
import com.africastalking.sms.Message;
import com.africastalking.sms.Recipient;
import com.africastalking.AfricasTalking;

import java.util.List;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class AfricasTalkingTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /* Set your app credentials */
	String USERNAME = "sandbox";
	String API_KEY = "16fc3e8992f3f4278e5fa98060a6900f933be33ffbac8e1333c86202b4dd3fc7";

	/* Initialize SDK */
	AfricasTalking.initialize(USERNAME, API_KEY);

	/* Get the SMS service */
	SmsService sms = AfricasTalking.getService(AfricasTalking.SERVICE_SMS);

	/* Set the numbers you want to send to in international format */
	String[] recipients = new String[] {
		"+256700155098"
	};

	/* Set your message */
	String message = "We really should work now!!!";
		
	/* That’s it, hit send and we’ll take care of the rest */
	try {
            List<Recipient> response = sms.send(message, recipients, true);
		for (Recipient recipient : response) {
			System.out.print(recipient.number);
			System.out.print(" : ");
			System.out.println(recipient.status);
		}
	} catch(Exception ex) {
		ex.printStackTrace();
	}
    }
    
}
