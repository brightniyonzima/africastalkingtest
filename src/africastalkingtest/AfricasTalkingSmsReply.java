package africastalkingtest;

import com.africastalking.Callback;
import com.africastalking.SmsService;
import com.africastalking.sms.Message;
import com.africastalking.sms.Recipient;
import com.africastalking.AfricasTalking;

import java.util.List;
import java.io.IOException;
/**
 *
 * @author lenovo
 */
public class AfricasTalkingSmsReply {
    
    public static void main(String[] args)
    {
        /* Set your app credentials */
	String USERNAME = "sandbox";
	String API_KEY = "16fc3e8992f3f4278e5fa98060a6900f933be33ffbac8e1333c86202b4dd3fc7";

	/* Initialize SDK */
	AfricasTalking.initialize(USERNAME, API_KEY);

	/* Get the SMS service */
	SmsService sms = AfricasTalking.getService(AfricasTalking.SERVICE_SMS);

	/* Set the numbers you want to send to in international format */
	String[] recipients = new String[] {
		"+256700155098"
	};

	/* Set your message */
	//String message = "We really should work now!!!";
        
        /*
            Our API will return 100 messages at a time back to you, starting with what you currently
            believe is the lastReceivedId. Specify 0 for the first time you access the method and
            the ID of the last message we sent you on subsequent calls
        */
        long lastReceivedId = 0;

		/* Fetch all messages using a loop */
	try {
            List<Message> messages;
            do {
                messages = sms.fetchMessages(lastReceivedId);
                for (Message message : messages) {
                    System.out.print(message.from);
                    System.out.print(" : ");
                    System.out.println(message.toString());

                    /* Reassign the lastReceivedId */
                    lastReceivedId = message.id;

                    /* NOTE: Be sure to save the lastReceivedId for next time */
                }
            } while(messages.size() > 0);
	} catch(Exception ex) {
		ex.printStackTrace();
	}
    }
}